import pyaudio
import sys
import time
import wave
import requests
 
chunk = 1024
FORMAT = pyaudio.paInt16
CHANNELS = 1

RATE = 16000

RECORD_SECONDS = 2
 

p = pyaudio.PyAudio()


input_device_index = 0

stream = p.open(format = FORMAT,
                channels = CHANNELS,
                rate = RATE,
                input = True,
                frames_per_buffer = chunk)
all = []
for i in range(0, int(RATE / chunk * RECORD_SECONDS)):
    data = stream.read(chunk)
    all.append(data)

stream.stop_stream()
stream.close()    
data = b''.join(all)                    
out = wave.open('wav/talk.wav','w')
out.setnchannels(1) #mono
out.setsampwidth(2) #16bits
out.setframerate(RATE)
out.writeframes(data)
out.close()
 
p.terminate()

path = 'wav/talk.wav'
url = "https://api.apigw.smt.docomo.ne.jp/amiVoice/v1/recognize?APIKEY={}".format('4636576532644d71512e68536b415849786b686678417a68324539417658734e6e3571734147552e426836')
files = {"a": open(path, 'rb'), "v":"on"}
r = requests.post(url, files=files)
print(r.json()['text'])

x = r.json()['text']
k = '\n'
file = open('talk.txt', 'w')
file.write(x)
file.write(k)
file.write(x)
file.close()
