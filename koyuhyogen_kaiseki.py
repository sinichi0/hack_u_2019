# coding utf-8

import requests
import json
import urllib
import pprint

kosi_flg = False

# f = open('talk.txt')    #ファイル読み込み
# data1 = f.read()   #ファイル終端まで全て読んだデータを返す
# f.close()



all_talks = data1.split('\n')   #改行で区切ったやつをall_talksに入れる

KEY = '4636576532644d71512e68536b415849786b686678417a68324539417658734e6e3571734147552e426836'   #APIキー
url = 'https://api.apigw.smt.docomo.ne.jp/gooLanguageAnalysis/v1/entity?APIKEY={}'.format(KEY)   #固有表現解析用

# res = requests.get(url)

payload = {
    "sentence": None,
    # "class_filter": "PSN"   #固有表現解析についての必要なフィルタを記述，区切る時は|でする
}

headers = {'Content-Type': 'application/json'}

payload["sentence"] = all_talks    #sentenceに区切られた文をぶち込む
r = requests.post(url, data=json.dumps(payload), headers=headers)


for name, _type in json.loads(r.text)['ne_list']:   #固有表現解析した結果を表示
    print("Entity: {} Type: {}".format(name, _type))
