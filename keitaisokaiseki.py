# coding utf-8

import requests
import json
import urllib
import pprint

kosi_flg = False

f = open('talk.txt')    #ファイル読み込み
data1 = f.read()   #ファイル終端まで全て読んだデータを返す
f.close()

all_talks = data1.split('\n')   #改行で区切ったやつをall_talksに入れる

KEY = '4636576532644d71512e68536b415849786b686678417a68324539417658734e6e3571734147552e426836'   #APIキー
URL = 'https://api.apigw.smt.docomo.ne.jp/gooLanguageAnalysis/v1/morph?APIKEY={}'.format(KEY)    #形態素解析用

RES = requests.get(URL)

payload = {
    "sentence": data1,
    "info_filter": "form|pos"   #形態素解析についての必要なフィルタを記述，区切る時は|でする
    # "class_filter": "PSN"   #固有表現解析についての必要なフィルタを記述，区切る時は|でする
}

headers = {'Content-Type': 'application/json'}

R = requests.post(URL, data=json.dumps(payload), headers=headers)


for name in json.loads(R.text)['word_list']:    #形態素解析した結果を表示
    print("Sentence: {}".format(name))
