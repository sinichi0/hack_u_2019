import pyaudio
import sys
import time
import wave
import requests
import threading
from text_operate import *
import numpy as np


def running():
	CHUNK = 1024
	FORMAT = pyaudio.paInt16
	CHANNELS = 1
	RATE = 48000
	RECORD_SECONDS = 8
	p = pyaudio.PyAudio()
	input_device_index = 0

	path = 'wav/talk.wav'
	url = "https://api.apigw.smt.docomo.ne.jp/amiVoice/v1/recognize?APIKEY={}".format('4636576532644d71512e68536b415849786b686678417a68324539417658734e6e3571734147552e426836')

	txt_opr = Txt_Operate('talk.txt')


	stream = p.open(format=FORMAT,
	                channels=CHANNELS,
	                rate=RATE,
	                input=True,
	                frames_per_buffer=CHUNK)

	#print("* recording")

	frames = []

	for i in range(0, int(RATE / CHUNK * RECORD_SECONDS)):
	    data = stream.read(CHUNK)
	    buf = np.frombuffer(data, dtype="int16") # 読み込んだストリームデータを2byteのInt型のリストに分離
	    frames.append(b''.join(buf[::3])) # 記録するデータを1/3に間引いてリストを結合してフレームに追加

	#print("* done recording")

	stream.stop_stream()
	stream.close()
	p.terminate()

	wf = wave.open('wav/talk.wav', 'wb')
	wf.setnchannels(CHANNELS)
	wf.setsampwidth(p.get_sample_size(FORMAT))
	wf.setframerate(RATE / 3) # ヘッダのサンプリングレートを16kHzにする
	wf.writeframes(b''.join(frames))
	wf.close()
			

	files = {"a": open(path, 'rb'), "v":"on"}
	r = requests.post(url, files=files)
	x = r.json()['text']
	#txt_opr.write_txt(x)


	tmp=''

	for i in range(len(x)):
		if x[i]=='。':
			txt_opr.write_txt(tmp)
			txt_opr.write_txt('\n')
			tmp = ''
		else:
			tmp=tmp+x[i]
