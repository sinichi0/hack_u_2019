# coding: UTF-8
#break_disisionとかいうファイル名のくせに，ただファイルオープンして，中身を持ってくるだけです．

f = open('talk.txt')     #ファイル読み込み
data1 = f.read()    #ファイル終端まで全て読んだデータを返す
f.close()   #ファイル閉じる

print(type(data1))    #文字列データ確認用（後で消しても良い）
print(data1)


lines1 = data1.split('\n') # 改行で区切る(改行文字そのものは戻り値のデータには含まれない) リスト型


print(type(lines1))
print(lines1)
for line in lines1:
    print(line)

# lines1["cluster"][""]
# # coding: UTF-8
# #break_disisionとかいうファイル名のくせに，ただファイルオープンして，中身を持ってくるだけです．
#
# f = open('talk.txt')     #ファイル読み込み
# data1 = f.read()    #ファイル終端まで全て読んだデータを返す
# f.close()   #ファイル閉じる
#
# print(type(data1))    #文字列データ確認用（後で消しても良い）
# print(data1)
#
# lines1 = data1.split('\n') # 改行で区切る(改行文字そのものは戻り値のデータには含まれない)
#
# print(type(lines1))
# print(lines1)
# for line in lines1:
#     print(line)


#!/usr/bin/env python
#-*- cording: utf-8 -*-

import pygame.mixer
import time

# kosi_flg = 0

if kosi_flg == True:
    # mixerモジュールの初期化
    pygame.mixer.init()
    # 音楽ファイルの読み込み
    pygame.mixer.music.load("Motion-Fracture03-1.mp3")
    # 音楽再生、および再生回数の設定(-1はループ再生)
    pygame.mixer.music.play(1)

    time.sleep(60)
    # 再生の終了
    pygame.mixer.music.stop()
