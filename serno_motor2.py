import RPi.GPIO as GPIO
import time

def motor():

    GPIO.setmode(GPIO.BCM)

    gp_out = 4
    GPIO.setup(gp_out, GPIO.OUT)
    motor = GPIO.PWM(gp_out, 50)
    motor.start(0.0)

    bot = 2.5
    mid = 7.2
    top = 12.0

    motor.ChangeDutyCycle(bot)
    time.sleep(0.5)

    motor.ChangeDutyCycle(mid)
    time.sleep(0.5)

    motor.ChangeDutyCycle(mid)
    time.sleep(0.5)

    motor.stop()
    GPIO.cleanup
