# coding utf-8

import requests
import json
import urllib
import pprint

f = open('talk.txt')    #ファイル読み込み
data1 = f.read()   #ファイル終端まで全て読んだデータを返す
f.close()

all_talks = data1.split('\n')   #改行で区切ったやつをall_talksに入れる

KEY = '4636576532644d71512e68536b415849786b686678417a68324539417658734e6e3571734147552e426836'   #APIキー
URL = 'https://api.apigw.smt.docomo.ne.jp/gooLanguageAnalysis/v1/morph?APIKEY={}'.format(KEY)    #形態素解析用
url = 'https://api.apigw.smt.docomo.ne.jp/gooLanguageAnalysis/v1/entity?APIKEY={}'.format(KEY)   #固有表現解析用
Url = 'https://api.apigw.smt.docomo.ne.jp/truetext/v1/clusteranalytics?APIKEY={}'.format(KEY)   #カテゴリ分析用
# URl = 'https://api.apigw.smt.docomo.ne.jp/truetext/v1/sensitivecheck?APIKEY={}'.format(KEY)   #センシティブ解析

# RES = requests.get(URL)
# res = requests.get(url)
Res = requests.get(Url)
# REs = requests.get(URl)

payload = {
    "sentence": None,
    "info_filter": "form|pos"   #形態素解析についての必要なフィルタを記述，区切る時は|でする
    # "class_filter": "PSN"   #固有表現解析についての必要なフィルタを記述，区切る時は|でする
}

headers = {'Content-Type': 'application/json'}
headerss = {'Content-Type': 'application/x-www-form-urlencoded'}

kei_enc = urllib.parse.urlencode({'sentence': all_talks})
kk = requests.post(URL, data=kei_enc, headers=headerss)

# payload["sentence"] = all_talks    #sentenceに区切られた文をぶち込む
# R = requests.post(URL, data=json.dumps(payload), headers=headers)
# r = requests.post(url, data=json.dumps(payload), headers=headers)

sen_enc = urllib.parse.urlencode({'text': all_talks})   #カテゴリ解析用
k = requests.post(Url, data=sen_enc, headers=headerss)

# sen = urllib.parse.urlencode({'text': all_talks})   #センシティブ解析用
# payload["text"] = all_talks
# kk = requests.post(URl, data=sen, headers=headerss)

# for name in json.loads(R.text)['word_list']:    #形態素解析した結果を表示
#     print("Sentence: {}".format(name))
print(json.loads(kk.text))

# for name, _type in json.loads(r.text)['ne_list']:   #固有表現解析した結果を表示
#     print("Entity: {} Type: {}".format(name, _type))

# for clusters in json.loads(k.text)['clusters']:   #カテゴリ分析した結果を表示
#     print("Category: {}".format(clusters))
# print(json.loads(k.text))

# for clusters in json.loads(kk.text)['rawdata']:   #センシティブ分析した結果を表示
#     print("Category: {}".format(clusters))
# print(json.loads(kk.text))

# response_dic = json.loads(k.text)
# print("status: " + response_dic['status'])
# print("cluster_name: " + response_dic['clusters'][0]['cluster_name'])
# print("cluster_name: " + response_dic['clusters'][1]['cluster_name'])

# response_dic2 = json.loads(kk.text)
# print("status: " + response_dic2['status'])
# print("cluster_name: " + response_dic2['rawdata'][0]['quotient_id'])

# num = 0
# if response_dic['status'] == 'OK':
#     while num <= 10:
#         if response_dic['clusters'][num-1]['cluster_name'] == response_dic['clusters'][num]['cluster_name']:
#             kosi_flg = False
#             num += 1
#         else:
#             kosi_flg = True
#             num += 1
#             break
#         else:
#             kosi_flg = False
# else:
#     kosi_flg = False

# print(kosi_flg)

# print(res.status_code)   #通信が成功してるか見るように作ったけど，403しか返ってこないよくわからない子...

