# coding utf-8

import requests
import json
import urllib
from text_operate import *

txt_opr = Txt_Operate('talk.txt')
txt_opr.make_txt()

talk_dic = txt_opr.read_txt()


category_sentence_encode = []   #カテゴリ解析用
one_category_response = []
category_response_dic = []

# form_sentence_encode = []   #形態素解析用
# one_form_responce = []
# form_response_dic = []

# unique_sentence_encode = []   #固有表現用
# one_unique_responce = []
# unique_response_dic = []

KEY = '4636576532644d71512e68536b415849786b686678417a68324539417658734e6e3571734147552e426836'   #APIキー
category_url = 'https://api.apigw.smt.docomo.ne.jp/truetext/v1/clusteranalytics?APIKEY={}'.format(KEY)   #カテゴリ分析用
form_url = 'https://api.apigw.smt.docomo.ne.jp/gooLanguageAnalysis/v1/morph?APIKEY={}'.format(KEY)    #形態素解析用
unique_url = 'https://api.apigw.smt.docomo.ne.jp/gooLang/uageAnalysis/v1/entity?APIKEY={}'.format(KEY)   #固有表現解析用

category_headers = {'Content-Type': 'application/x-www-form-urlencoded'}
form_headers = {'Content-Type': 'application/json'}
unique_headers = {'Content-Type': 'application/json'}
# old_count = 0
lines_count = 0
ex_word = []

def kosiori_hantei():

    global lines_count
    kosi_flg = False
    recent_lines = len(talk_dic)

    while lines_count < recent_lines:

        category_sentence_encode.append(urllib.parse.urlencode({'text': talk_dic[lines_count]}))
        # print(category_sentence_encode[lines_count])
        one_category_response.append(requests.post(category_url, category_sentence_encode[lines_count], headers = category_headers))
        # print(one_category_response[lines_count])
        category_response_dic.append(json.loads(one_category_response[lines_count].text))
        # print(category_response_dic[lines_count])

        # form_sentence_encode.append(urllib.parse.urlencode({'text': talk_dic[lines_count]}))  # 形態素解析用
        # one_form_responce.append(requests.post(form_url, form_sentence_encode[lines_count], headers = form_headers))
        # # form_lines = one_form_responce[lines_count][0:3]
        # form_response_dic.append(json.loads(one_form_responce[lines_count].text))
        # # form_lines = form_response_dic[lines_count][0:3]
        # ex_word[lines_count-1] = "".join(talk_dic[lines_count])
        # ex_word[lines_count-1] = ex_word[0:4]


        # unique_sentence_encode.append(urllib.parse.urlencode({'text': talk_dic[lines_count]}))  # 固有表現用
        # one_unique_responce.append(requests.post(unique_url, unique_sentence_encode[lines_count], headers = unique_headers))
        # unique_response_dic.append(json.loads(one_unique_responce[lines_count].text))

        # print("デバッグ: ")
        # print(lines_count)
        if lines_count+1 != recent_lines:
            lines_count += 1

    if lines_count < 0:
        lines_count = 0

    if 3 < recent_lines:
        for num in range(2):
            # if ex_word[lines_count] == "ところで":
            #     kosi_flg = False
            #     return False
            # if category_response_dic[recent_lines - num - 2]['status'] == 'OK' and category_response_dic[recent_lines-1]['status'] == 'OK':
            if True:
                if category_response_dic[recent_lines - num - 2]['clusters'][0]['cluster_name'] == category_response_dic[recent_lines-1]['clusters'][0]['cluster_name']:
                    kosi_flg = False
                    # if ex_word == "ところで":
                    #     kosi_flg = False
                    # else:
                    #     if unique_response_dic[recent_lines - num - 1]['ne_list'][0] == unique_response_dic[recent_lines - 1]['ne_list'][0]:
                    #         kosi_flg = False
                    #     else:
                    #         # kosi_flg = True
                    #         return True
                else:
                    kosi_flg = True
                    return True
                    break
            else:
                kosi_flg = False

    else:
        kosi_flg = False


    return kosi_flg