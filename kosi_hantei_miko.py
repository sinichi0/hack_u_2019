# coding utf-8
#こしおり判定 made by 御子貝

import requests
import json
import urllib


# all_talks = data1.split('\n')   #改行で区切ったやつをall_talksに入れる



# res = requests.get(unique_url)   #固有表現用
# RES = requests.get(category_url)   #カテゴリ分析用
#
# payload = {
#     "sentence": None,
#     # "class_filter": "PSN"   #固有表現解析についての必要なフィルタを記述，区切る時は|でする
# }




def kosi_miko(self):
    kosi_flg = False

    KEY = '4636576532644d71512e68536b415849786b686678417a68324539417658734e6e3571734147552e426836'  # APIキー
    # unique_url = 'https://api.apigw.smt.docomo.ne.jp/gooLanguageAnalysis/v1/entity?APIKEY={}'.format(KEY)  # 固有表現解析用
    category_url = 'https://api.apigw.smt.docomo.ne.jp/truetext/v1/clusteranalytics?APIKEY={}'.format(KEY)  # カテゴリ分析用

    # unique_headers = {'Content-Type': 'application/json'}
    category_headers = {'Content-Type': 'application/x-www-form-urlencoded'}

    # payload["sentence"] = all_talks  # sentenceに区切られた文をぶち込む
    # unique_res = requests.post(unique_url, data=json.dumps(payload), headers=headers)

    f = open('talk.txt')  # ファイル読み込み
    lines = f.readlines()
    f.close()
    talks = []
    response_dic = []
    category_sen = []

    old_lines = 0

    recent_lines = sum(1 for lines in open('talk.txt'))
    lines_count = old_lines
    while lines_count < recent_lines:
        talks.append(lines[lines_count])
        lines_count += 1

    old_lines = lines_count = recent_lines

    # category_sen = urllib.parse.urlencode({'text': talks[0]})  # カテゴリ解析用
    category_sen.append(urllib.parse.urlencode({'text': talks[0]}))
    category_res = requests.post(category_url, data=category_sen, headers=category_headers)

    # response_dic = json.loads(category_res.text)
    response_dic.append(json.loads(category_res.text))

    print("status: " + response_dic['status'])
    print("cluster_name: " + response_dic['clusters'][0]['cluster_name'])

    num = recent_lines - 3

    if response_dic['status'] == 'OK':
        while num < recent_lines:
            if response_dic[num]['clusters'][num]['cluster_name'] == response_dic[]['clusters'][recent_lines]['cluster_name']:
                kosi_flg = False

            elif response_dic['clusters'][num]['cluster_name'] == response_dic['clusters'][recent_lines]['cluster_name']:
                kosi_flg = False

            else:
                kosi_flg = True
                break
            num += 1
    else:
        kosi_flg = False

    print(kosi_flg)

    return  kosi_flg