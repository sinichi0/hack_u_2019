class Txt_Operate:

	def __init__(self, txt_name):
		self.txt_name = txt_name


	def make_txt(self):

		try:
			file = open(self.txt_name, 'r')
			file.close()
		except FileNotFoundError as e:
			file = open(self.txt_name, 'w')
			file.close()


	# スズキ担当（）
	def write_txt(self, take_txt):  # 書き込む文字でーた（?）はtake_txt

		self.take_txt = take_txt

		f = open(self.txt_name, 'a')
		f.write(take_txt)
		f.close()


	def delete(self):
		del self.txt_name


	def clear(self):
		self.txt_name.clear()


	# 白倉担当
	def read_txt(self):

		txt_data = {}

		with open(self.txt_name) as f:
			file = [s.strip() for s in f.readlines()]

			for i in range(len(file)):
				txt_data[i] = {file[i]}

		return txt_data


# //samplecode//
#
# import text_operate for Txt_Operate
#
# txt_opr = Txt_Operate('sample.txt')
#
# txt_opr.make_txt()
# txt_opr.write_txt("適当に")